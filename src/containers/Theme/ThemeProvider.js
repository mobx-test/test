import React from 'react';
import { PropTypes } from 'prop-types';
import { inject, observer } from 'mobx-react';
import { create } from 'jss';
import rtl from 'jss-rtl';
import Loading from 'react-loading-bar';
import {
    withTheme, withStyles,
    createMuiTheme, MuiThemeProvider
} from '@material-ui/core/styles';
import { StylesProvider, jssPreset } from '@material-ui/styles';

import applicationTheme from '../../utils/theme';


const styles = {
    root: {
        width: '100%',
        minHeight: '100%',
        marginTop: 0,
        zIndex: 1,
    }
};

// Configure JSS
const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

export const AppContext = React.createContext();

class ThemeProvider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pageLoaded: true,
            theme: createMuiTheme(applicationTheme(props.color, props.mode, props.direction)),
        };
    }

    componentWillMount = () => {
        this.onProgressShow();
    }

    componentDidMount = () => {
        this.playProgress();
    }

    componentWillUnmount() {
        this.onProgressShow();
    }

    onProgressShow = () => {
        this.setState({ pageLoaded: true });
    }

    onProgressHide = () => {
        this.setState({ pageLoaded: false });
    }

    playProgress = () => {
        this.onProgressShow();
        setTimeout(() => {
            this.onProgressHide();
        }, 500);
    }

    handleChangeMode = mode => {
        const { color, changeMode, direction } = this.props;
        this.setState({ theme: createMuiTheme(applicationTheme(color, mode, direction)) });
        changeMode(mode);
    };

    render(){
        const {
            classes,
            children,
        } = this.props;
        const { pageLoaded, theme } = this.state;
        return(
            <StylesProvider jss={jss}>
                <MuiThemeProvider theme={theme}>
                    <div className={classes.root}>
                        <div className={classes.pageLoader}>
                            <Loading
                                show={pageLoaded}
                                color={theme.palette.primary.main}
                                showSpinner={false}
                            />
                        </div>
                        <AppContext.Provider value={this.handleChangeMode}>
                            {children}
                        </AppContext.Provider>
                    </div>
                </MuiThemeProvider>
            </StylesProvider>
        )
    }
}

ThemeProvider.propTypes = {
    view: PropTypes.object.isRequired,
    children: PropTypes.element.isRequired,
    color: PropTypes.string.isRequired,
    mode: PropTypes.string.isRequired,
    direction: PropTypes.string.isRequired,
    changeMode: PropTypes.func.isRequired,
};

export default inject('view')(observer(withStyles(styles)(ThemeProvider)));
