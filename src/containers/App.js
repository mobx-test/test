import React from 'react';
import DevTools from 'mobx-react-devtools';

import StoreProvider from './StoreProvider'
import Theme from './Theme';
import { Wrapper, DrawerSwitcher, Bar, Content } from './Layout';

const App = ({ store }) => (
  <StoreProvider store={store}>
    <>
      {process.env !== 'production' && <DevTools />}
      <Theme>
        <Wrapper>
          TEST
        </Wrapper>
      </Theme>
    </>
  </StoreProvider>
);

export default App;
