import { configure } from 'mobx';

import ViewStore from './ViewStore';
import RouterStore from './RouterStore';
import ThemeStore from './ThemeStore';

configure({ enforceActions: 'observed' });

export default class AppStore {
  constructor() {
    this.view = new ViewStore(this);
    this.router = new RouterStore(this);
    this.theme = new ThemeStore(this);
  }
}
