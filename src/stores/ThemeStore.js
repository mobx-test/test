import { decorate, observable, action, computed } from 'mobx';

import { getScreenWidth } from '../utils/browser';
import { capitalize } from '../utils/text';

class ThemeStore {
  constructor(appStore) {
    this.appStore = appStore;
  }

  toggleTemChange = () => {
    this.changeMode = !this.changeMode;
  }

}

decorate(ThemeStore, {
  toggleTemChange: action,
});

export default ThemeStore;
